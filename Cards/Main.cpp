// struct for card games

#include <iostream>
#include <conio.h>


enum Rank // ace high
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit
{
	SPADE, DIAMOND, CLUB, HEART
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	std::cout << "The ";

	if (card.rank >= 2 && card.rank <= 10)
	{
		std::cout << card.rank;
	}

	else
	{
		if (card.rank == JACK) std::cout << "Jack";
		if (card.rank == QUEEN) std::cout << "Queen";
		if (card.rank == KING) std::cout << "King";
		if (card.rank == ACE) std::cout << "Ace";
	}

	std::cout << " of ";

	if (card.suit == SPADE) std::cout << "Spades";
	if (card.suit == DIAMOND) std::cout << "Diamonds";
	if (card.suit == CLUB) std::cout << "Clubs";
	if (card.suit == HEART) std::cout << "Hearts";
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank) return(card1);
	else return(card2);
}

int main()
{
	// testing

	Card card1;
	card1.rank = SIX;
	card1.suit = HEART;

	Card card2;
	card2.rank = QUEEN;
	card2.suit = SPADE;

	PrintCard(card1);
	std::cout << "\n" ;

	PrintCard(card2);
	std::cout << "\n";

	PrintCard(HighCard(card1, card2));
	std::cout << " is the Higher Card";
	std::cout << "\n";

	_getch();
	return 0;
}
